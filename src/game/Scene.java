/*
 * To change this license header, choose License Headers in Project Properties.
 */
package game;

import java.lang.Math.*;

/*
 */
public class Scene {
    private int height;
    private int width;
    private String[][] terrain;

    public Scene (int height, int width) {
        this.height = height;
        this.width = width;
        terrain = new String[height][width];        // <1>
    }

    public int getHeight () {
        return height;
    }
    
    public int getWidth () {
        return width;
    }
    
    public void addTerrain(String type, int row, int column) {
        terrain[row][column] = type;  // <2>
    }

    public String getTerrain(int row, int column) {
        return terrain[row][column];                // <3>
    }
    
    public int distance(int row1, int col1, int row2, int col2) {
        int distance = 0;
        if( row1 == row2)
            distance = Math.abs((col2 - col1));
        else if( col1 == col2)
            distance =  Math.abs((row2 - row1));
        else
            distance =  Math.abs(Math.abs(col2 - col1) + Math.abs(row2 - row1));
        return distance;
    }
    
    /** Check if a given cell is empty */
public boolean isEmpty(int row, int column) {
    return terrain[row][column] == null;
}

/** Count the cells which are not empty */
public int countItems(int row, int column, int maxDistance) {
    int count = 0;
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            if (!isEmpty(i, j)) {
                //System.out.println(i + " " + j);
                if ( distance(i, j, row, column) <= maxDistance) {
                    count += 1;
                    System.out.println(i + " " + j + " count: " + count);
                }
            }
        }
    }

    return count;
}
}
