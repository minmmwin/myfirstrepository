/*
 * Task 2 - 6
 */
package game;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.lang.Math;

/**
 *
 * @author minmm
 */
public class SceneTest {
    
    /**
     * Test of getHeight method, of class Scene.
     */
    @Test
    public void testGetHeight() {
        Scene scene = new Scene(0, 0);
        assertEquals(0, scene.getHeight());
        assertEquals(0, scene.getWidth());
        Scene scene2 = new Scene(10, 5);
        assertEquals(10, scene2.getHeight());
        assertEquals(5, scene2.getWidth());
    }
    
    @Test
    public void testAddTerrain() {
        Scene scene = new Scene (5, 3);
        scene.addTerrain("G", 2, 1);
        scene.addTerrain("T", 4, 2);
        assertEquals("G", scene.getTerrain(2, 1));
        assertEquals("T", scene.getTerrain(4, 2));
    }
    
    @Test
    public void testDistance() {
        Scene scene = new Scene (3, 5);
        scene.addTerrain("A", 0, 1);
        scene.addTerrain("B", 0, 2);
        scene.addTerrain("C", 2, 1);
        scene.addTerrain("D", 2, 4);
        assertEquals("A", scene.getTerrain(0, 1));
        assertEquals("B", scene.getTerrain(0, 2));
        assertEquals("C", scene.getTerrain(2, 1));
        assertEquals("D", scene.getTerrain(2, 4));
        assertEquals(1, scene.distance(0, 1, 0, 2));
        assertEquals(2, scene.distance(0, 1, 2, 1));
        assertEquals(5, scene.distance(0, 1, 2, 4));
        
        assertEquals(1, scene.distance(0, 2, 0, 1));
        assertEquals(2, scene.distance(2, 1, 0, 1));
        assertEquals(5, scene.distance(2, 4, 0, 1));    
        
        assertEquals(4, scene.distance(0, 1, 1, 4));
        assertEquals(3, scene.distance(0, 2, 1, 4));
        assertEquals(4, scene.distance(2, 1, 1, 4));
    }
    
    @Test
    public void testisEmpty() {
        Scene scene = new Scene (3, 5);
        assertEquals(true, scene.isEmpty(0, 0));  
        scene.addTerrain("A", 0, 1);
        assertEquals(false, scene.isEmpty(0, 1));  
    }
    
    @Test
    public void testCountItems() {
        Scene scene = new Scene (3, 5);
        scene.addTerrain("A", 0, 1);
        scene.addTerrain("B", 0, 2);
        scene.addTerrain("C", 2, 1);
        scene.addTerrain("D", 2, 4);
        assertEquals(1, scene.countItems(0, 0, 1));
        assertEquals(2, scene.countItems(0, 0, 2));
        assertEquals(3, scene.countItems(0, 0, 3));
        assertEquals(3, scene.countItems(0, 0, 4));
        assertEquals(3, scene.countItems(0, 0, 5));
        assertEquals(4, scene.countItems(0, 0, 6));
        
        assertEquals(1, scene.countItems(1, 4, 1));
        assertEquals(1, scene.countItems(1, 4, 2));
        assertEquals(2, scene.countItems(1, 4, 3));
        assertEquals(4, scene.countItems(1, 4, 4));
    }
}
